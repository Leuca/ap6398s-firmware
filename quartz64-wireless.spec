# Based on Manjaro package 'ap6398s-firmware' and 'firmware-raspberrypi'
# More at https://gitlab.manjaro.org/manjaro-arm/packages/community/ap6256-firmware
# and at https://gitlab.manjaro.org/manjaro-arm/packages/community/firmware-raspberrypi

%define debug_package %{nil}

%global _bluezcommit a6d4105d0989103ea6384b65940c76add1914127

Name:           quartz64-wireless
Version:        2023.01
Release:        4%{?dist}
Summary:        Firmware files for CYW43455 and CYW43456
License:        unknown

ExclusiveArch:  aarch64

Requires:       linux-firmware >= 20220708-136

Source0:        https://github.com/RPi-Distro/bluez-firmware/raw/%{_bluezcommit}/broadcom/BCM4345C0.hcd
Source1:        https://github.com/RPi-Distro/bluez-firmware/raw/%{_bluezcommit}/broadcom/BCM4345C5.hcd
Source2:        brcmfmac43456-sdio.AP6256.txt
Source3:        brcmfmac43456-sdio.clm_blob
Source4:        fw_bcm43456c5_ag.bin

%description
Files needed by some rockchip boards that use CYW43455 and CYW43456 chips

%prep

%build

%install
mkdir -p %{buildroot}%{_prefix}/lib/firmware/brcm

# Bluetooth firmwares
install -m 0644 %{SOURCE0} %{buildroot}%{_prefix}/lib/firmware/brcm
install -m 0644 %{SOURCE1} %{buildroot}%{_prefix}/lib/firmware/brcm

# Wi-Fi firmwares
install -m 0644 %{SOURCE2} %{buildroot}%{_prefix}/lib/firmware/brcm
install -m 0644 %{SOURCE3} %{buildroot}%{_prefix}/lib/firmware/brcm
install -m 0644 %{SOURCE4} %{buildroot}%{_prefix}/lib/firmware/brcm/brcmfmac43456-sdio.bin

# Create links
ln -s brcmfmac43456-sdio.AP6256.txt %{buildroot}%{_prefix}/lib/firmware/brcm/brcmfmac43456-sdio.txt

# This links are not provided by linux-firmware
ln -s brcmfmac43455-sdio.AW-CM256SM.txt.xz %{buildroot}%{_prefix}/lib/firmware/brcm/brcmfmac43455-sdio.pine64,soquartz-cm4io.txt.xz
ln -s brcmfmac43455-sdio.AW-CM256SM.txt.xz %{buildroot}%{_prefix}/lib/firmware/brcm/brcmfmac43455-sdio.txt.xz

%files
%{_prefix}/lib/firmware/brcm/BCM4345C0.hcd
%{_prefix}/lib/firmware/brcm/BCM4345C5.hcd
%{_prefix}/lib/firmware/brcm/brcmfmac43456-sdio.AP6256.txt
%{_prefix}/lib/firmware/brcm/brcmfmac43456-sdio.txt
%{_prefix}/lib/firmware/brcm/brcmfmac43455-sdio.txt.xz
%{_prefix}/lib/firmware/brcm/brcmfmac43456-sdio.bin
%{_prefix}/lib/firmware/brcm/brcmfmac43456-sdio.clm_blob
%{_prefix}/lib/firmware/brcm/brcmfmac43455-sdio.pine64,soquartz-cm4io.txt.xz


%changelog
* Sat Oct 14 2023 Luca Magrone <luca@magrone.cc> - 2023.01-4
- Add brcmfmac43455-sdio.txt.xz link

* Sat Feb 04 2023 Luca Magrone <luca@magrone.cc> - 2023.01-3
- Require at least linux-firmware version 20220708-136
- Link soquartz txt against firmware in linux-firmware

* Wed Feb 01 2023 Luca Magrone <luca@magrone.cc> - 2023.01-2
- Rename package again to make it less misleading

* Wed Feb 01 2023 Luca Magrone <luca@magrone.cc> - 2023.01-1
- Rename package
- Support both CYW43455 and CYW43456 chips

* Tue Jan 31 2023 Luca Magrone <luca@magrone.cc> - 2020.02-4
- Remove links
- Avoid compression

* Tue Jan 31 2023 Luca Magrone <luca@magrone.cc> - 2020.02-3
- Drop wifi firmware since it's in linux-firmware
- Remove links not relevant to pine64
- Compress with crc32

* Mon Jan 30 2023 Luca Magrone <luca@magrone.cc> - 2020.02-2
- Compress firmware files

* Mon Jan 30 2023 Luca Magrone <luca@magrone.cc> - 2020.02-1
- Initial package release
